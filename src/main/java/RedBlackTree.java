// https://en.wikipedia.org/wiki/Red%E2%80%93black_tree it's some complex, so we just hold on.
/**
 * left is less, right is more
 * @param <T>
 */
public class RedBlackTree<T extends Comparable<T>> {
    public Node<T> root;

    public void insert(T data) {
        // ok, how can we insert?

        // insert into the right place.
        // how did we insert. do you think?
        // find the parent??
        Node<T> n = new Node<T>(data);
        if (root == null) {
            this.root = n;
            balanceInsert(null, n, false);
        } else {
            insert(n);
        }
    }

    /**
     * @param isLeft true is insert to left, false otherwise
     */
    private void balanceInsert(Node<T> p, Node<T> n, boolean isLeft) {
        if (p == null) {    // case 1, insert is the root, just paint the root to black
            n.setBlack();
            return;
        }


    }

    private void insert(Node<T> n) {
        boolean isLeft = false;
        Node<T> p = root;

        while (p != null) {
            if (p.compare(n) <= 0 && (p.left != null) && p.left.compare(n) <= 0) { // can go left
                p = p.left;
            } else if (p.compare(n) > 0 && (p.right != null) && p.right.compare(n) > 0) {
                p = p.right;
            } else {
                break;
            }
        }
        assert (p != null);
        // now we find the insert parent.
        Node<T> old;
        if (p.compare(n) <= 0) {
            old = p.left;
            p.setLeft(n);
            isLeft = true;
        } else {
            old = p.right;
            p.setRight(n);
        }
        if (old != null) {
            if (old.compare(n) <= 0) {
                n.setLeft(old);
            } else {
                n.setRight(old);
            }
        }
        balanceInsert(p, n, isLeft);
    }

    private void dumpTraverse(Node<T> root, int level, StringBuilder sb) {
        if (root == null) return;
        StringBuilder space = new StringBuilder();
        for (int i = 0; i < level; i++) {
            space.append("  ");
        }
        sb.append(space);
        sb.append(root.value);
        dumpTraverse(root.left, level + 1, sb);
    }

    // yes we want dump this tree.
    public String dump() {
        StringBuilder sb = new StringBuilder();
        dumpTraverse(this.root, 0, sb);
        return sb.toString();
    }
}

class Node<T extends Comparable<T>> {
    public Node<T> left;
    public Node<T> right;
    public Node<T> parent;
    T value;
    private boolean isRed = true;

    public Node(T value) {
        this.value = value;
    }

    public void setRed() {
        this.isRed = true;
    }

    public void setBlack() {
        this.isRed = false;
    }

    public boolean isRed() {
        return isRed;
    }

    public boolean isBlack() {
        return !isRed();
    }

    public void setLeft(Node<T> n) {
        this.left = n;
        n.parent = this;
    }

    public void setRight(Node<T> n) {
        this.right = n;
        n.parent = this;
    }

    public Node<T> grandFather() {
        if (this.parent != null) {
            return this.parent.parent;
        }
        return null;
    }

    public Node<T> uncle() {
        Node<T> grandFather = this.grandFather();
        if (grandFather.left == this.parent) {
            return grandFather.right;
        } else if (grandFather.right == this.parent) {
            return grandFather.left;
        }
        return null;
    }

    public int compare(Node<T> n) {
        return this.value.compareTo(n.value);
    }
}